﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Statistics" Type="Folder">
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Auto Column Width.vi" Type="VI" URL="../Auto Column Width.vi"/>
				<Item Name="Count If True.vi" Type="VI" URL="../Count If True.vi"/>
				<Item Name="Is True.vi" Type="VI" URL="../Is True.vi"/>
				<Item Name="Organize Events Table.vi" Type="VI" URL="../Organize Events Table.vi"/>
				<Item Name="Organize into Tree Data.vi" Type="VI" URL="../Organize into Tree Data.vi"/>
				<Item Name="Organize Methods Table.vi" Type="VI" URL="../Organize Methods Table.vi"/>
				<Item Name="Organize Properties Table.vi" Type="VI" URL="../Organize Properties Table.vi"/>
			</Item>
			<Item Name="Class Statistics.vi" Type="VI" URL="../Class Statistics.vi"/>
			<Item Name="Method Statistics.vi" Type="VI" URL="../Method Statistics.vi"/>
			<Item Name="Property Statistics.vi" Type="VI" URL="../Property Statistics.vi"/>
			<Item Name="VI Server Class Hierarchy.vi" Type="VI" URL="../VI Server Class Hierarchy.vi"/>
		</Item>
		<Item Name="VI Server Toolkit.lvlib" Type="Library" URL="../VI Server Toolkit.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="ASC_ArrayOfUniqueIDStringParentIDStringName.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASC_ArrayOfUniqueIDStringParentIDStringName.ctl"/>
				<Item Name="ASC_UniqueIDStringParentIDStringName.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASC_UniqueIDStringParentIDStringName.ctl"/>
				<Item Name="ASPM_ArrayOfUniqueIDStringDatanameShortnameLongname.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASPM_ArrayOfUniqueIDStringDatanameShortnameLongname.ctl"/>
				<Item Name="ASPM_UniqueIDStringDatanameShortnameLongname.ctl" Type="VI" URL="/&lt;vilib&gt;/_script/All Supported PropertiesOrMethods TypeDefs/ASPM_UniqueIDStringDatanameShortnameLongname.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
